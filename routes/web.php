<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Route::get('/login/user', 'Auth\LoginController@showUserLoginForm');
Route::get('/register/user', 'Auth\RegisterController@showUserRegisterForm');
Route::post('/login/user', 'Auth\LoginController@userLogin');
Route::post('/register/user', 'Auth\RegisterController@createUser');

Route::group(['middleware' => 'auth:user'], function () {
    Auth::routes();
    Route::view('/user', 'user');
});


Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::group(['middleware' => 'auth:admin'], function () {
    Auth::routes();
    Route::view('/admin', 'admin');
});
